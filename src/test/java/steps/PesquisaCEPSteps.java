package steps;

import java.util.Map;

import org.openqa.selenium.WebDriver;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pageobjects.PaginaInicial;
import pageobjects.PaginaResultado;
import utils.PropertyReader;
import webdriver.DriverFactory;

public class PesquisaCEPSteps {

	WebDriver driver = DriverFactory.getChromeDriver();

	@Dado("^que estou na pagina inicial do site dos Correios$")
	public void que_estou_na_pagina_inicial_site_dos_Correios() throws Throwable {
		driver.get(PropertyReader.getProperty("url"));
	}

	@Quando("^eu digitar o CEP/Endereco no campo de busca$")
	public void eu_digitar_o_CEP_no_campo_de_busca(DataTable dataTable) throws Throwable {
		PaginaInicial paginaInicial = new PaginaInicial(driver);
		for (Map<String, String> map : dataTable.asMaps(String.class, String.class)) {
			paginaInicial.pesquisaEndereco(map.get(map.keySet().toArray()[0]));
		}
	}

	@Entao("^os dados do endereco serao exibidos com sucesso$")
	public void os_dados_do_endereco_serao_exibidos_com_sucesso() throws Throwable {
		PaginaResultado paginaResultado = new PaginaResultado(driver);
		paginaResultado.getResultadosPesquisa();
	}

}
