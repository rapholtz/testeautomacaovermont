#author: rafael.matoso@outlook.com
#language: pt
@TesteAnalistaAutomacao
Funcionalidade: Pesquisa de endereco no site dos Correios

  Contexto: 
    Dado que estou na pagina inicial do site dos Correios

  @Teste01 @PesquisaCEP
  Cenario: Pesquisar CEP e imprimir as informacoes encontradas
    Quando eu digitar o CEP/Endereco no campo de busca
      | CEP      |
      | 06654760 |
    Entao os dados do endereco serao exibidos com sucesso

  @Teste02 @PesquisaLogradouro
  Cenario: Pesquisar logradouro e imprimir todos os dados da tabela retornada
    Quando eu digitar o CEP/Endereco no campo de busca
      | Logradouro |
      | Barueri    |
    Entao os dados do endereco serao exibidos com sucesso
