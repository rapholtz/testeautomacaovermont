package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

public class PaginaResultado {

	WebDriver driver;

	public PaginaResultado(WebDriver driver) {
		this.driver = driver;
	}

	public void getResultadosPesquisa() {
		TabelaResultados tabelaResultados = new TabelaResultados();
		driver.switchTo().window(driver.getWindowHandles().toArray()[1].toString());
		By botaoProximaPagina = By.xpath("//*[contains(text(),'[ Pr�xima ]')]");
		boolean proximaPagina = true;
		while (proximaPagina) {
			getResultadosTabela(tabelaResultados);
			try {
				if (!driver.findElement(botaoProximaPagina).getAttribute("href").isEmpty()) {
					driver.findElement(botaoProximaPagina).click();
				}
			} catch (NullPointerException nullPointerException) {
				proximaPagina = false;
			} catch (NoSuchElementException noSuchElementException) {
				proximaPagina = false;
				System.out.println("\nDADOS NAO ENCONTRADOS");
			}
		}
		imprimeResultadosTabela(tabelaResultados);
	}

	private void getResultadosTabela(TabelaResultados tabelaResultados) {
		tabelaResultados.setLogradouroNome(driver.findElements(By.xpath("//tbody/tr/td[1]")));
		tabelaResultados.setBairroDistrito(driver.findElements(By.xpath("//tbody/tr/td[2]")));
		tabelaResultados.setLocalidadeUF(driver.findElements(By.xpath("//tbody/tr/td[3]")));
		tabelaResultados.setCep(driver.findElements(By.xpath("//tbody/tr/td[4]")));
	}

	private void imprimeResultadosTabela(TabelaResultados tabelaResultados) {
		System.out.println(tabelaResultados.toString());
	}

}
