package pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

public class TabelaResultados {

	private List<String> logradouroNome = new ArrayList<String>();
	private List<String> bairroDistrito = new ArrayList<String>();
	private List<String> localidadeUF = new ArrayList<String>();
	private List<String> cep = new ArrayList<String>();

	public void setLogradouroNome(List<WebElement> logradouroNome) {
		for (WebElement element : logradouroNome) {
			this.logradouroNome.add(element.getText().replaceAll("\n", " ").replace("  ", " - ").trim());
		}
	}

	public void setBairroDistrito(List<WebElement> bairroDistrito) {
		for (WebElement element : bairroDistrito) {
			this.bairroDistrito.add(element.getText().trim());
		}
	}

	public void setLocalidadeUF(List<WebElement> localidadeUF) {
		for (WebElement element : localidadeUF) {
			this.localidadeUF.add(element.getText().trim());
		}
	}

	public void setCep(List<WebElement> cep) {
		for (WebElement element : cep) {
			this.cep.add(element.getText().trim());
		}
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < cep.size(); i++) {
			stringBuilder
					.append("Logradouro/Nome: " + logradouroNome.get(i) + ", Bairro/Distrito: " + bairroDistrito.get(i)
							+ ", Localidade/UF: " + localidadeUF.get(i) + ", CEP: " + cep.get(i) + "\n");
		}
		return stringBuilder.toString();
	}

}
