package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class PaginaInicial {

	private final By campoBusca = By.id("acesso-busca");

	WebDriver driver;

	public PaginaInicial(WebDriver driver) {
		this.driver = driver;
	}

	public void pesquisaEndereco(String endereco) {
		driver.findElement(campoBusca).click();
		driver.findElement(campoBusca).clear();
		driver.findElement(campoBusca).sendKeys(endereco);
		driver.findElement(campoBusca).sendKeys(Keys.ENTER);
	}

}
