package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import webdriver.DriverFactory;

public class Hooks {

	@Before
	public void beforeScenario(Scenario scenario) {
		System.out.println("Iniciando a execu��o do teste: " + scenario.getName() + "\n");
	}

	@After
	public void afterScenario(Scenario scenario) {
		System.out.println("\nFinalizando a execu��o do teste: " + scenario.getName() + " com o status: "
				+ scenario.getStatus() + "\n");
		DriverFactory.quitDriver();
	}
}
